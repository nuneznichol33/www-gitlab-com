---
layout: job_family_page
title: "SEC & SOX Reporting or Corporate Controller"
---

## Levels

### Senior SEC Analyst 

#### Senior SEC Analyst Job Grade

The Senior SEC Analyst is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior SEC Analyst Job Responsbilities

* Assist with the preparation of external financial reporting and supporting schedules, including S-1, 10Q, 10K, 8K and quarterly investor slides, and other financial reporting as required.
* Assist in earnings release process including preparation of the quarterly management reporting package and review of financial information included in the earnings release and script.
* Prepare the disclosure checklist; assist with XBRL review process.
* Prepare journal entries and reconciliations in assigned areas (e.g. stock-based compensation and stock repurchase); assist with preparation of equity rollforward, EPS calculation and other PBC schedules as required.
* Assist in ensuring compliance with SEC and U.S. GAAP reporting requirements; assist in the implementation of new accounting pronouncements.
* Assist with researching, resolving and documenting accounting issues that arise during the quarter.
* Ensure Sarbanes-Oxley requirements are met for relevant areas (e.g. disclosure and equity accounting controls).
* Assist with internal and external reporting packages preparation as needed, including key investor reporting.
* Provide assistance with special projects as required.

#### Senior SEC Analyst Job Requirements

* BA/BS degree in Accounting; CPA or in the process of completing the licensing preferred.
* Minimum of 5-7 years of relevant experience at a public Company; 
* Highly detail oriented, organized, self-motivated and proactive. Must have strong work ethic and be able to meet critical deadlines.
* Strong analytical and problem-solving skills, including the ability to research and interpret accounting guidance.
* Strong written and verbal communications skills.
* Excellent team player who can work effectively with cross-functional teams.
* Proficiency in Word, Excel and PowerPoint required.
* Knowledge and proficient experience with WDesk and Netsuite highly preferred.

### Director, SEC & SOX

The Director, SEC & SOX reports to the [Corporate Controller](https://about.gitlab.com/job-families/finance/corporate-controller/#director-corporate-controller).

#### Director, SEC & SOX Job Grade

The Director, SEC & SOX is a [grade #10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, SEC & SOX Responsibilities

- Responsible for SOX review of compliance to controls for monthly/quarterly/yearly controls. The position will review the tests completed by the Internal Audit team and drive necessary changes as required by the Accounting team. 
- Responsible for the integrity and accuracy of GitLab monthly and quarterly financial filings as if it were a public company.
- Oversight and participation in planning, drafting and other activities that facilitate the efficient completion of those filings. 
- Such activities include: (i) planning the quarterly drafting and review cycle; (ii) leading quarterly meetings with senior management; (iii) developing an in-depth understanding and analysis of significant transactions to assess their impact for SEC disclosure purposes; and (iv) monitoring the activities of all accounting standard setting bodies to ensure timely identification of emerging accounting pronouncements to ensure compliance with all GAAP and disclosure requirements.
- Partners closely with the Technical Accounting side of the team, occasionally participating in technical accounting analysis of significant or unusual transactions, or recently issued accounting updates.
- Interacts regularly with external auditors, Legal and Investor Relations, with routine exposure to the CFO and Audit Committee.

#### Director, SEC & SOX Requirements

- 10 years of work experience with a Bachelor’s Degree or at least 8 years of work experience 
- An advanced degree with an active CPA license is preferred
- Bachelor's Degree in Accounting or Finance
- Certified Public Accountant
- Minimum of 8-10 years of relevant work experience, including Big 4 public accounting experience and SEC reporting experience, with a large publicly-traded company.
- Strong technical knowledge of U.S. GAAP and SEC accounting and reporting requirements.
- Excellent communication skills, both written and verbal.
- Experience with and handling of SOX requirements and manager of SOX review with 3-5 years of experience.  As part of this requirement, has worked with the internal audit team driving and reviewing testing.
- Demonstrated knowledge and experience researching and documenting disclosure guidance and the accounting treatment of issues.
- Ability to effectively facilitate and present in group meetings.
- Strong analytical, problem-solving, and strategic-thinking skills with a forward-looking focus.
- Teamwork is essential to the group, so it is critical that the candidate interact with other team members and managers collaboratively and effectively.
- Excellent time management, organizational, and project management skills with experience in working toward tight deadlines.
- Experience with SOX 404 and internal control over financial reporting.
- Advanced proficiency with Workiva and XBRL.
- Demonstrated professional commitment, initiative, accountability and ownership of assignments.
- Driven to add value and continuously seek opportunities for improvement both individually and as a team.
- Comfortable working in a fast-paced environment and adept at handling change.
- Can effectively work in an all remote environment. 
- Ability to use GitLab.

## Performance Indicators

1. [Average days to close](/handbook/finance/accounting/#average-days-to-close-kpi-definition)
1. [Number of material audit adjustments](/handbook/internal-audit/#performance-measures-for-accounting-related-to-audit)
1. [Average days of sales outstanding](/handbook/finance/accounting/#11-accounts-receivable)
1. [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
1. [Percentage of ineffective SOX Controls](/handbook/internal-audit/#performance-measures-for-accounting-related-to-audit)

## Career Ladder

The next step in the Corporate Controller job family is to move to a senior leader or executive leadership job family which would be the PAO role or similar.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Meet with PAO for 50 minutes
- Meet with Key Finance Leaders - for 30 minutes each
- Meet with Director of Legal for 40 minutes
- Meet with VP of Finance for 50 minutes
- Meet with CFO for 50 minutes

Additional details about our process can be found on our [hiring page](/handbook/hiring).
